<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# GitLab for Software Development in Teams

This workshop builds upon the HIFIS Workshops [Introduction to Git and GitLab](https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material) and [Bring Your Own Script and Make It Ready for Publication](https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/workshop-materials)
but focuses on the collaboration aspects of Git and GitLab.
You will get a deeper understanding on Git concepts such as branching, merging and rebasing, which are necessary to know if you use Git in a team setup.

In addition, you learn further concepts and practices for software development in teams to improve the change process of your software.
In this context, we make use of GitLab features such as issues, merge requests, and build pipelines.
You learn how to apply these features on the basis of a [consistent example project](https://gitlab.com/hifis/hifis-workshops/software-development-for-teams/sample-calculator) which follows an iterative development approach.

## Prerequisites

- Basic knowledge about Git and a collaboration platform such as GitLab or GitHub.
- Basic knowledge in using a programming language such as Python, R, or Matlab.
- On your computer you require a recent Web browser, a text editor, and a recent Git client.

## Curriculum

Here you find the curriculum of this workshop which is laid out for two full workshop days.
The timings might vary depending on the interests of the workshop participants.
If you want to set up a workshop based on this materials, please see the [Workshop Setup Information](00_setup/README.md).

| Estimated Time | Topic |
| ------ | ------ |
| 90min | [Introdution - Introduction to Software Processes and Change Management](01_introduction) |
| 30min | [Git for Teams - Introduction and Quiz](02_git-for-teams/episodes/episode01.md) |
| 30min | [Git for Teams - Preparation](02_git-for-teams/episodes/episode02.md) |
| 60min | [Git for Teams - Branching and Merging](02_git-for-teams/episodes/episode03.md) |
| 45min | [Git for Teams - Rebasing](02_git-for-teams/episodes/episode04.md) |
| 45min | [Git for Teams - Conflicts](02_git-for-teams/episodes/episode05.md) |
| 30min | [Example Project - Introduction - Explain the Project Context and our Tasks](03_example-project/episodes/episode01.md) |
| 60min | [Example Project - Iteration 2 - Set up the Issue Tracker](03_example-project/episodes/episode02.md) |
| 20min | [Example Project - Iteration 2 - Introduction to Build Automation](03_example-project/episodes/episode03.md) |
| 70min | [Example Project - Iteration 2 - Set up the Build Pipeline](03_example-project/episodes/episode04.md) |
| 15min | [Example Project - Iteration 2 - Close Iteration 2](03_example-project/episodes/episode05.md) |
| 120min | [Example Project - Iteration 3 - Perform Iteration 3](03_example-project/episodes/episode06.md) |
| 30min | [Example Project - Iteration 3 - Release Version 1.0.0](03_example-project/episodes/episode07.md) |
| 45min | [Closing - Reflect your own Development Workflow](04_closing/README.md) |

## Contributors

Here you find the main contributors to the material:

- Tobias Schlauch
- Martin Stoffers
- Michael Meinel
- Benjamin Wolff

## Contributing

Please see [the contribution guidelines](CONTRIBUTING.md) for further information about how to contribute.

## Changes

Please see the [Changelog](CHANGELOG.md) for notable changes of the material.

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.
