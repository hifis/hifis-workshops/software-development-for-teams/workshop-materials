<!--
SPDX-FileCopyrightText: 2021 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Closing

- Objective: Think about improvements of your own development process
- Estimated time: 45min
- Teaching techniques: Interactive Exercise

## Exercise - What about your Development Process? 

Think about your development process (exercise in the beginning of the workshop): 
What aspects would you change / apply after taking this workshop?
