<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Example Project

In the following, we demonstrate different GitLab features in context of a software project:
- You can find related slides in the [slides directory](slides).
- You can find related code examples in the [code directory](code).
- You can find the complete example project showing all important steps here: https://gitlab.com/hifis/hifis-workshops/software-development-for-teams/sample-calculator 
  (branch `02a-initial-design` is the starting point for iteration 2,
  branch `03a-prepare-iteration-3` is the starting point for iteration 3).

## Set the Scene

- [Explain the Project Context and our Tasks](episodes/episode01.md)

## Iteration 2 - Starting in the middle of the Project

- [Set up the Issue Tracker](episodes/episode02.md)
- [Introduction to Build Automation](episodes/episode03.md)
- [Set up the Build Pipeline](episodes/episode04.md)
- [Close Iteration 2](episodes/episode05.md)

## Iteration 3 - Finish the initial Release

- [Perform Iteration 3](episodes/episode06.md)
- [Release Version 1.0.0](episodes/episode07.md)

## Bonus

- [Bugfix Releases using Release Branches and Tags](episodes/episode08.md)

## Further Readings

- [DLR Software Engineering Guidelines](https://rse.dlr.de/guidelines/00_dlr-se-guidelines_en.html)
- [Martin Fowler: Patterns for Managing Source Code Branches](https://martinfowler.com/articles/branching-patterns.html)
